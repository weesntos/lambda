# README #

Deploy de Lambda + API Gateway para ambientes de DEV, TEST, PREPROD e PROD com Serverless

### How do I get set up? ###

Deploy automatizado de código para ambientes com SERVERLESS

* Deploy de Lambda
* Deploy de S3
* Deploy de Api Gateway
* Deploy CloudFormation

### Contribution guidelines ###

Se for executado a primeira vez, necessário deploy sem alias (ambiente)
serverless deploy --stage dev

Para deploy em ambiente especifico com Serverless
```
serverless deploy -s dev --alias dev
```
```
serverless deploy -s dev --alias tester
```
```
serverless deploy -s dev --alias preprod
```

Publica em producao
```
serverless deploy --stage v1
```